﻿namespace TP_Seance2
{
    partial class FrmLesKatas
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
			this.btnValider = new System.Windows.Forms.Button();
			this.lblPlayer = new System.Windows.Forms.Label();
			this.txtValeur = new System.Windows.Forms.TextBox();
			this.lblDebut = new System.Windows.Forms.Label();
			this.tbResult = new System.Windows.Forms.TextBox();
			this.lblGeneration = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// btnValider
			// 
			this.btnValider.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnValider.Location = new System.Drawing.Point(251, 64);
			this.btnValider.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.btnValider.Name = "btnValider";
			this.btnValider.Size = new System.Drawing.Size(232, 25);
			this.btnValider.TabIndex = 0;
			this.btnValider.Text = "Valider";
			this.btnValider.UseVisualStyleBackColor = true;
			this.btnValider.Click += new System.EventHandler(this.btnValider_Click);
			// 
			// lblPlayer
			// 
			this.lblPlayer.AutoSize = true;
			this.lblPlayer.Location = new System.Drawing.Point(11, 7);
			this.lblPlayer.Name = "lblPlayer";
			this.lblPlayer.Size = new System.Drawing.Size(57, 16);
			this.lblPlayer.TabIndex = 1;
			this.lblPlayer.Text = "Fizzbuzz";
			// 
			// txtValeur
			// 
			this.txtValeur.Location = new System.Drawing.Point(36, 67);
			this.txtValeur.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.txtValeur.Name = "txtValeur";
			this.txtValeur.Size = new System.Drawing.Size(209, 22);
			this.txtValeur.TabIndex = 2;
			this.txtValeur.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValeur_KeyPress);
			// 
			// lblDebut
			// 
			this.lblDebut.AutoSize = true;
			this.lblDebut.Location = new System.Drawing.Point(24, 36);
			this.lblDebut.Name = "lblDebut";
			this.lblDebut.Size = new System.Drawing.Size(190, 16);
			this.lblDebut.TabIndex = 3;
			this.lblDebut.Text = "Entrer un choffre entre 15 et 150";
			// 
			// tbResult
			// 
			this.tbResult.Location = new System.Drawing.Point(36, 163);
			this.tbResult.Multiline = true;
			this.tbResult.Name = "tbResult";
			this.tbResult.ReadOnly = true;
			this.tbResult.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.tbResult.Size = new System.Drawing.Size(447, 247);
			this.tbResult.TabIndex = 4;
			this.tbResult.Text = "\r\n";
			// 
			// lblGeneration
			// 
			this.lblGeneration.AutoSize = true;
			this.lblGeneration.Location = new System.Drawing.Point(33, 135);
			this.lblGeneration.Name = "lblGeneration";
			this.lblGeneration.Size = new System.Drawing.Size(127, 16);
			this.lblGeneration.TabIndex = 5;
			this.lblGeneration.Text = "Génération fizzbuzz :";
			// 
			// FrmLesKatas
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ControlLight;
			this.ClientSize = new System.Drawing.Size(520, 441);
			this.Controls.Add(this.lblGeneration);
			this.Controls.Add(this.tbResult);
			this.Controls.Add(this.lblDebut);
			this.Controls.Add(this.txtValeur);
			this.Controls.Add(this.lblPlayer);
			this.Controls.Add(this.btnValider);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.Name = "FrmLesKatas";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "FizzBuzz";
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnValider;
        private System.Windows.Forms.Label lblPlayer;
        private System.Windows.Forms.TextBox txtValeur;
		private System.Windows.Forms.Label lblDebut;
		private System.Windows.Forms.TextBox tbResult;
		private System.Windows.Forms.Label lblGeneration;
	}
}

