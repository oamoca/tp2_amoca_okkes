using FizzBuzzGenerator;
using System.Reflection.Emit;
using Xunit;

namespace FizzBuzzTest
{
	public class FizzBuzzTest
	{
		private Generator generator;

		public FizzBuzzTest()
		{
			generator = new Generator();
		}

		[Fact]
		public void TestMultiple3()
		{
			string expectedValue = "1 2 Fizz ";
			string generatedValue = generator.Generate(3);
			Assert.Equal(expectedValue, generatedValue);
		}

		[Fact]
		public void TestMultiple5()
		{
			string expectedValue = "1 2 Fizz 4 Buzz ";
			string generatedValue = generator.Generate(5);
			Assert.Equal(expectedValue, generatedValue);
		}

		[Fact]
		public void TestMultiple3Et5()
		{
			string expectedValue = "1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz ";
			string generatedValue = generator.Generate(15);
			Assert.Equal(expectedValue, generatedValue);
		}
	}
}