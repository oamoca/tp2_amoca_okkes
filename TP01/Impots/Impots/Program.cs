﻿using Impots;
using System.Reflection.Emit;

const int MINIMUM = 0;
ImpotsRevenu impotsRevenu = new ImpotsRevenu();

int input=0;
while (true)
{
	while (!(input < MINIMUM))
    {
		Console.WriteLine("Entrer votre revenu annuel supérieur à 0 : \n >>");
		try
		{
			input = Int32.Parse(Console.ReadLine());
		}
		catch (Exception)
		{
			input = 0;
		}

		double impots = impotsRevenu.CalculerImpots(input);
		double tauxImpots = impotsRevenu.TauxImpots(input);
		Console.WriteLine($"L'impot sur le revenu (taux {tauxImpots}) : \n{impots} EU\n");
	}
}