﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tennis
{
	public class Joueur
	{
		public readonly List<int> scores = new List<int> { 0, 15, 30, 40 };
		private int score = 0;
		public bool Avantage;
		public int Id;
		private int partie = 0;

		public Joueur(int id)
		{
			Id = id;
		}

		public int Score
		{
			get { return score; }
			private set { score = value; }
		}

		public int Parties
		{
			get { return partie; }
			private set { partie = value; }
		}

		public void PointGagne()
		{
			AugmenterScore();
		}

		public void JeuGagne()
		{
			Avantage = false;
			AugmenterJeu();
			ResetScore();
		}

		public void JeuPerdu()
		{
			Avantage = false;
			ResetScore();
		}

		public void AvantagePerdu()
		{
			Avantage = false;
		}
		public void AvantageGagne()
		{
			Avantage = true;
		}

		public void AugmenterScore()
		{
			int newScoreIndex = scores.FindIndex(possibleScore => possibleScore.Equals(score)) + 1;
			int newScore = scores.ElementAt(newScoreIndex);
			Score = newScore;
		}
		
		public void AugmenterJeu()
		{
			Parties++;
		}

		public void ResetScore()
		{
			Score = scores.First();
		}

		public bool IsScoreMax()
		{
			if (Score == scores.Last())
				return true;
			return false;
		}
	}
}
