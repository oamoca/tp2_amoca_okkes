﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CreditImmobilier
{
	public class Credit
	{
		private double _montant { get; set; }
		private int _duree { get; set; }
		private Taux _taux { get; set; }
		private Assurance _assurance { get; set; }

		public Credit(double montant, int duree, int tauxStatus, int tauxDuree)
		{
			this._montant = montant;
			this._duree = duree;
			this._taux = new Taux(tauxStatus, tauxDuree);
		}

		public void SetAssurance(Assurance assurance)
		{
			this._assurance = assurance;
		}

		public double GetMensualite()
		{
			double taux = _taux.GetTaux / 100;
			double tauxMensuel = taux / 12;
			int nbMois = _duree * 12;
			double mensualite = (_montant * tauxMensuel) / (1 - Math.Pow(1 + tauxMensuel, -nbMois));
			return mensualite;
		}

		public double GetTaux()
		{
			return this._taux.GetTaux;
		}

		public double GetMontantTotal()
		{
			return GetMensualite() * _duree * 12;
		}

		public double GetMontantInteret()
		{
			return GetMontantTotal() - _montant;
		}

		public double GetMontantAssurance()
		{
			double tauxAssurance = _assurance.GetTauxAssurance();
			double montantAssurance = _montant * tauxAssurance;
			return montantAssurance;
		}

		public double GetMontantAssuranceMensuelle()
		{
			return GetMontantAssurance() / (_duree * 12);
		}

		public double GetMontantTotalAvecAssurance()
		{
			return GetMontantTotal() + GetMontantAssurance();
		}

		public double GetMontantTotalAvecAssuranceMensuelle()
		{
			return GetMontantTotalAvecAssurance() / (_duree * 12);
		}
	}
}
