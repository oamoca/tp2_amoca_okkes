﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Immobilier
{
	public class Assurance
	{
		private double _tauxActuel;

		private const double _tauxBase = 0.003;
		private const double _tauxSportif = -0.0005;
		private const double _tauxFumeur = 0.0015;
		private const double _tauxTroublesCardiaques = 0.003;
		private const double _tauxIngenieurInformatique = -0.0005;
		private const double _tauxPiloteDeChasse = 0.0015;

		public Assurance()
		{
			init();
		}

		private void init()
		{
			this._tauxActuel += _tauxBase;
		}

		public void EstSpotif(bool status)
		{
			if (status)
				this._tauxActuel += _tauxSportif;
		}

		public void EstFumeur(bool status)
		{
			if (status)
				this._tauxActuel += _tauxFumeur;
		}

		public void EstTroublesCardiaques(bool status)
		{
			if (status)
				this._tauxActuel += _tauxTroublesCardiaques;
		}

		public void EstIngenieurInformatique(bool status)
		{
			if (status)
				this._tauxActuel += _tauxIngenieurInformatique;
		}

		public void EstPiloteDeChasse(bool status)
		{
			if (status)
				this._tauxActuel += _tauxPiloteDeChasse;
		}

		public double GetTauxAssurance()
		{
			return this._tauxActuel;
		}
	}
}
