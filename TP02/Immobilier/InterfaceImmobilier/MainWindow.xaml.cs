﻿using CreditImmobilier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InterfaceImmobilier
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		List<CheckBox> AssuranceCheckbox = new List<CheckBox>();

		public MainWindow()
		{
			InitializeComponent();

			AssuranceCheckbox.Add(chbFumeur);
			AssuranceCheckbox.Add(chbSportif);
			AssuranceCheckbox.Add(chbTroubleCardiaque);
			AssuranceCheckbox.Add(chbIngenieurInformatique);
			AssuranceCheckbox.Add(chbPiloteChasse);
		}

		private void tbMontantEmprunt_PreviewTextInput(object sender, TextCompositionEventArgs e)
		{
			ToolBox.ControlText(e);
		}

		private void tbDureeEmprunt_PreviewTextInput(object sender, TextCompositionEventArgs e)
		{
			ToolBox.ControlText(e);
		}

		private void btCalculer_Click(object sender, RoutedEventArgs e)
		{
			ControlValidation();
		}

		private void ControlValidation()
		{
			Assurance assurance = new Assurance();
			assurance.SetSportif(chbSportif.IsChecked);
			assurance.SetFumeur(chbFumeur.IsChecked);
			assurance.SetTroublesCardiaques(chbTroubleCardiaque.IsChecked);
			assurance.SetIngenieurInformatique(chbIngenieurInformatique.IsChecked);

			bool assure = false;
			foreach (CheckBox chb in AssuranceCheckbox)
			{
				if (chb.IsChecked == true) { assure = true; break; }
			}

			string montantEmprunt = tbMontantEmprunt.Text;
			string dureeRemboursement = tbDureeRemboursement.Text;
			string typeDeTaux = cbTypeTaux.Text;
			string dureeEmprunt = cbDureeEmprunt.Text;

			bool testMontant = ToolBox.ControlMontant(montantEmprunt);
			bool testDureeRemboursement = ToolBox.ControlDuree(dureeRemboursement);
			bool testTaux = ToolBox.ControlTaux(typeDeTaux, dureeEmprunt);

			if (testMontant && testDureeRemboursement && testTaux)
			{
				Credit credit = new Credit(ToolBox.ConvertStringToDouble(montantEmprunt), ToolBox.ConvertStringToInt(dureeRemboursement), ToolBox.ConvertStringToInt(typeDeTaux), ToolBox.ConvertStringToInt(dureeEmprunt));
				if (assure)
					credit.SetAssurance(assurance);

				tbTaux.Text = credit.GetTaux().ToString();

				tbMontantMensualite.Text = Math.Round(credit.GetMensualite(), 2).ToString();
				tbMontantTotalMensualite.Text = Math.Round(credit.GetMensualite(), 2).ToString();
				tbMontantInteret.Text = Math.Round(credit.GetMontantInteret(), 2).ToString();

				if (assure)
				{
					tbMontantAssurance.Text = Math.Round(credit.GetMontantAssurance(), 2).ToString();
					tbMontantTotalAvecAssurance.Text = Math.Round(credit.GetMontantTotalAvecAssurance(), 2).ToString();
					tbMontantAssuranceMensuelle.Text = Math.Round(credit.GetMontantAssuranceMensuelle(), 2).ToString();
					tbMontantTotalMensualite.Text = Math.Round(credit.GetMontantTotalAvecAssuranceMensuelle(), 2).ToString();
				}
				else
				{
					tbMontantAssurance.Text = "0";
					tbMontantAssuranceMensuelle.Text = "0";
					tbMontantTotalAvecAssurance.Text = Math.Round(credit.GetMontantTotal(), 2).ToString();
				}
			}
		}
	}
}
